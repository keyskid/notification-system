
export const routes = [
    {
        path: '/',
        name: 'index',
        component: () => import('/imports/client/pages/index.vue').then((c) => c.default),
    },
];
