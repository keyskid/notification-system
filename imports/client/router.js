import VueRouter from 'vue-router';
import {routes} from '/imports/client/routes.js';

export const router = new VueRouter({
    routes,
    mode: 'history',
});
