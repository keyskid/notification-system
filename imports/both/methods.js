import {Notifications} from '/imports/both/collections';

Meteor.methods({
    sendNotification({documentName, documentStatus, targetUserIds}) {
        check(documentName, String);
        check(documentStatus, String);
        check(targetUserIds, [String]);

        // if (!['success', 'info', 'warning', 'error'].includes(documentStatus)) {
        if (!['approved', 'rejected'].includes(documentStatus)) {
            throw new Meteor.Error('sendNotification-invalid_document_status', 'Invalid document status');
        }

        const sendingUserId = this.userId;

        // This shouldn't happen
        if(!sendingUserId) throw new Meteor.Error('sendNotification-not_logged_in', 'You must be logged in to send a notification');

        const created_at = new Date();

        Notifications.insert({sendingUserId, documentName, documentStatus, targetUserIds, created_at});
    },
});
