
export const Notifications = new Mongo.Collection('notifications');

if (Meteor.isServer) {
  Notifications._ensureIndex({targetUserIds: 1});
}