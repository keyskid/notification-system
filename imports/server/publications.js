import {Notifications} from '/imports/both/collections';

Meteor.publish('notifications', function() {
    const userId = this.userId;
    if (!userId) {
        return this.ready();
    }

    return Notifications.find({targetUserIds: this.userId}, {
        fields: {
            _id: 1,
            documentName: 1,
            documentStatus: 1,
            'targetUserIds.$': 1,
            created_at: 1,
        },
    });
});

Meteor.publish('userList', function() {
    return Meteor.users.find({}, {fields: {_id: 1}});
});