import '/imports/both/methods';
import '/imports/server/publications';

Meteor.users.deny({update: () => true});

Meteor.startup(() => {
    console.log('Meteor Started');
});