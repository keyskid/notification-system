import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import {router} from '/imports/client/router.js';
import LayoutVue from '/imports/client/pages/Layout.vue';
import 'vuetify/dist/vuetify.min.css';

Vue.use(VueRouter);
Vue.use(Vuetify);

const vuetify = new Vuetify({
  theme: {
    dark: true,
  }
});

Meteor.startup(() => {
  new Vue({
    el: '#app',
    router,
    vuetify,
    render: h => h(LayoutVue),
  });
});
