# Notification System

This is built using MeteorJS and Vue, persistence is handled with a MongoDB Database

When you land on the site, it will create you a new anonymous user
account, so if you wish to test with multiple users just open a 
different browser or an incognito window.

The API is set up using Meteor's Pub/Sub and Method system. 
Notifications will be displayed in real time.